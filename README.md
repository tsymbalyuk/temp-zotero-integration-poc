This is a POC of exporting an item to Zotero library using oAuth capability.

### Workflow:

- Read https://www.zotero.org/support/dev/web_api/v3/oauth and register the app (i.e. Inspire website/application) using link https://www.zotero.org/oauth/apps (you need to have an account with Zotero);
- Upon registration, use `apiKey` and `apiSecret` to configure the application;
- To test the application, go to http://localhost:9801/authorize
- Enter your Zotero account credentials and accept the addition of the accesToken/key to your account (this is what end users will do);
- Zotero will redirect back to application, which will use the provided accessToken to perform a write action `add a book` to user's library (will add a dummy book item);
- The JSON of items in the library will be return result on the screen.

### Considerations:
- The oAuth token request and exchange is handled via external library;
- When going to authorization endpoint, the app also requests write permissions to the user's library.
- The business calls to Zotero are not oAuth1.0a-compliant, they just attach token as query parameter.

### To do:
- In real-world application user's tokens need to be stored in order not to request authorization each time user wants to export to Zotero;
- For unauthorized users there are options - either request token each time, or store tokens temporarily and just check uer's credentials (use special param to request auth page)

