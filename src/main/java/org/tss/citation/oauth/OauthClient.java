package org.tss.citation.oauth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.oauth.OAuth10aService;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.tss.citation.model.Creator;
import org.tss.citation.model.Item;
import org.tss.citation.model.Tag;

@Component
public class OauthClient {

  private static final Logger log = LoggerFactory.getLogger("oAuth client");

  private static final String apiKey = "dd0058a0a873c800f075";
  private static final String apiSecret = "148181fcfcc54c3736be";

  @Autowired
  private RestTemplate restTemplate;
  @Autowired
  private ObjectMapper objectMapper;

  private OAuth10aService service;
  private OAuth1RequestToken requestToken;

  public String attemptAuthorization(HttpServletRequest request)
      throws InterruptedException, ExecutionException, IOException {
    String callbackUrl = ServletUriComponentsBuilder.fromCurrentContextPath().path("process")
        .build().toUriString();
    log.info("Callback url: {}", callbackUrl);

    service = new ServiceBuilder(apiKey).apiSecret(apiSecret).callback(callbackUrl)
        .build(new ZoteroApi());

    requestToken = service.getRequestToken();
    log.info("Request token: {}", requestToken.getToken());

    String authUrl = service.getAuthorizationUrl(requestToken);
    authUrl = addFeaturesToUrl(authUrl);
    log.info("Auth URL: {}", authUrl);

    return authUrl;
  }

  private String addFeaturesToUrl(String url) {
    return UriComponentsBuilder.fromUriString(url).queryParam("write_access", "1").build()
        .toUriString();
  }

  public String processAuthorization(String verifier)
      throws InterruptedException, ExecutionException, IOException {
    OAuth1AccessToken accessToken = service.getAccessToken(requestToken, verifier);
    String userId = accessToken.getParameter("userID");
    log.info("Access token: {}  userId: {}", accessToken.getToken(), userId);

    URI url = UriComponentsBuilder.fromUriString("https://api.zotero.org/users/{userId}/items")
        /*.queryParam("key", accessToken.getToken())*/.build(userId);
    log.info("Business URL: {}", url.toString());

    HttpHeaders headers = new HttpHeaders();
    headers.add("Zotero-API-Key", accessToken.getToken());
    headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

    List<Item> items = List.of(prepareItem());

    log.info(objectMapper.writeValueAsString(items));

    ResponseEntity<String> response;
    try {
      response = restTemplate
          .exchange(url, HttpMethod.POST, new HttpEntity<>(items, headers), String.class);
    } catch (RestClientException e) {
      log.error("Exception: {}; Cause: {}", e.getLocalizedMessage(), e.getCause());
      return e.getLocalizedMessage();
    }

    return response.getBody();
  }

  private Item prepareItem() {
    return new Item("book", "My awesome book",
        List.of(new Creator("author", "Jessie", "Skinner"),
            new Creator("author", "Samantha Folksy")),
        List.of(new Tag("awesome"), new Tag("must read", 1)));
  }
}
