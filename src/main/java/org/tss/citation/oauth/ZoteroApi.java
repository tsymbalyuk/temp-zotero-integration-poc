package org.tss.citation.oauth;

import com.github.scribejava.core.builder.api.DefaultApi10a;

public class ZoteroApi extends DefaultApi10a {

  @Override
  public String getRequestTokenEndpoint() {
    return "https://www.zotero.org/oauth/request";
  }

  @Override
  public String getAccessTokenEndpoint() {
    return "https://www.zotero.org/oauth/access";
  }

  @Override
  protected String getAuthorizationBaseUrl() {
    return "https://www.zotero.org/oauth/authorize";
  }
}
