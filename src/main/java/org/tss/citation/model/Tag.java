package org.tss.citation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Tag {

  private String tag;
  private Integer type;

  public Tag() {
  }

  public Tag(String tag) {
    this.tag = tag;
  }

  public Tag(String tag, Integer type) {
    this.tag = tag;
    this.type = type;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
}
