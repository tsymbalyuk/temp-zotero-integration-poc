package org.tss.citation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;

@JsonInclude(Include.NON_NULL)
public class Item {

  private String itemType;
  private String title;
  private List<Creator> creators;
  private List<Tag> tags;

  public Item() {
  }

  public Item(String itemType, String title, List<Creator> creators,
      List<Tag> tags) {
    this.itemType = itemType;
    this.title = title;
    this.creators = creators;
    this.tags = tags;
  }

  public String getItemType() {
    return itemType;
  }

  public void setItemType(String itemType) {
    this.itemType = itemType;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<Creator> getCreators() {
    return creators;
  }

  public void setCreators(List<Creator> creators) {
    this.creators = creators;
  }

  public List<Tag> getTags() {
    return tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }
}

