package org.tss.citation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Creator {

  private String creatorType;
  private String name;
  private String firstName;
  private String lastName;

  public Creator() {
  }

  public Creator(String creatorType, String name) {
    this.creatorType = creatorType;
    this.name = name;
  }

  public Creator(String creatorType, String firstName, String lastName) {
    this.creatorType = creatorType;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getCreatorType() {
    return creatorType;
  }

  public void setCreatorType(String creatorType) {
    this.creatorType = creatorType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
