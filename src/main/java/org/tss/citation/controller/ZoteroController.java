package org.tss.citation.controller;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tss.citation.oauth.OauthClient;

@RestController
public class ZoteroController {

  @Autowired
  private OauthClient oauthClient;

  @GetMapping("/authorize")
  public void attemptAuthorization(HttpServletRequest request, HttpServletResponse response)
      throws InterruptedException, ExecutionException, IOException {
    response.setHeader(HttpHeaders.LOCATION, oauthClient.attemptAuthorization(request));
    response.setStatus(HttpStatus.FOUND.value());
  }

  @GetMapping(value = "/process", produces = MediaType.APPLICATION_JSON_VALUE)
  public String processAuthorization(@RequestParam("oauth_token") String requestToken,
      @RequestParam("oauth_verifier") String verifier)
      throws InterruptedException, ExecutionException, IOException {
    return oauthClient.processAuthorization(verifier);
  }
}
